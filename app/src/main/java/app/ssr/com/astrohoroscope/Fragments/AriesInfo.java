package app.ssr.com.astrohoroscope.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.ssr.com.astrohoroscope.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AriesInfo extends Fragment {
    TextView[] text;
    ImageView imageCeleb;

    TextView info;
    String datas;

    TextView title;
    String[] data;
    Typeface typeNormal,typeBold;
    public AriesInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewRoot = inflater.inflate(R.layout.fragment_aries_info, container, false);

        typeNormal = Typeface.createFromAsset(getContext().getAssets(), "fonts/mon.otf");
        typeBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/FredokaOne_Regular.ttf");
        title = (TextView) viewRoot.findViewById(R.id.signName);
        imageCeleb = (ImageView) viewRoot.findViewById(R.id.celeb_pics);

        title.setText("Aries(The Ram)");
        title.setTypeface(typeBold);

        data = getResources().getStringArray(R.array.ariesInfo);
        int[] textID ={R.id.element_info,R.id.quality_info,R.id.color_info,R.id.ruler_info,
                R.id.stone_info,R.id.day_info,R.id.great_info,R.id.number_info,R.id.date_info,
                R.id.secret_info,R.id.spot_info,R.id.strength_info,R.id.weakness_info,R.id.likes_info,
                R.id.dislike_info};

        text = new TextView[textID.length];

        info = (TextView) viewRoot.findViewById(R.id.info_info);
        datas = getResources().getString(R.string.aries_info);
        info.setTypeface(typeNormal);
        info.setText(datas);
        imageCeleb.setImageResource(R.drawable.aries_celeb);
        for(int i =0; i<data.length; i++){
            text[i] = (TextView) viewRoot.findViewById(textID[i]);
            }

        for (int a =0;a<text.length;a++){
            text[a].setText(data[a]);
            text[a].setTypeface(typeNormal);
        }


        return viewRoot;
    }

}
