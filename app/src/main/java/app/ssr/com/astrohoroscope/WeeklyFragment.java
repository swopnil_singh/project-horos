package app.ssr.com.astrohoroscope;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyFragment extends Fragment{

    String url;

    //static final String url = "http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Aries&id=45";
    static final String KEY_ITEM = "item";
    static final String KEY_TITLE = "title";
    static final String KEY_DESCRIPTION = "description";
    static final String KEY_LINK = "link";

    private ProgressDialog progressDialog;
    ListView listView;
    Typeface typeBold,typeNormal;
    ArrayList<HashMap<String,String>> menuItems;
    SimpleDateFormat simple;

    int imageArray[] = {R.drawable.two_star,R.drawable.two_half_star,R.drawable.three_star
    ,R.drawable.three_half_star,R.drawable.four_star,R.drawable.four_half_star};
    ImageView image;
    Random random;

    public WeeklyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly,container,false);

        if(hasConnection(getActivity())) {

        menuItems = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.weekly_list);


       typeBold = Typeface.createFromAsset(getContext().getAssets(),"fonts/mon.otf");
       typeNormal = Typeface.createFromAsset(getContext().getAssets(), "fonts/FredokaOne_Regular.ttf");



        Bundle bundle = this.getArguments();
        url = bundle.getString("weeklyUrl");

        GetItems getItems = new GetItems();
        getItems.execute();
        }else{
            showNetDisabledAlertToUser(getActivity());
        }

        return view;
    }
    private class GetItems extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading data....");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            XmlParser parser = new XmlParser();
            String xml = parser.getXmlFromUrl(url);
            Document doc = parser.getDomElement(xml);

            NodeList n1 = doc.getElementsByTagName(KEY_ITEM);
            //looped all items
            for(int i = 0;i < n1.getLength();i++){
                HashMap<String, String> map = new HashMap<>();
                Element e = (Element) n1.item(i);
                //adding child to hashmap
                map.put(KEY_TITLE,parser.getValue(e,KEY_TITLE));
                map.put(KEY_DESCRIPTION,parser.getValue(e,KEY_DESCRIPTION));
                map.put(KEY_LINK,parser.getValue(e,KEY_LINK));
                //adding hashmap to array list
                menuItems.add(map);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            ListAdapter adapter = new SimpleAdapter(getActivity(), menuItems,R.layout.row_tabs,
                    new String[]{KEY_TITLE,KEY_DESCRIPTION},
                    new int[]{R.id.feed_date,R.id.feed_data})

            {
                @Override
                public View getView(int pos, View convertView, ViewGroup parent) {
                    View v = convertView;
                    if (v == null) {
                        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        v = vi.inflate(R.layout.row_tabs, null);
                    }
                    TextView tv = (TextView) v.findViewById(R.id.feed_date);

                    String[] dates = (menuItems.get(pos).get(KEY_TITLE)).split(" ");

                    String [] first = dates[3].split("/");
                    String [] second = dates[5].split("/");
                    int f = Integer.parseInt(first[0]) - 1;
                    int s = Integer.parseInt(second[0]) - 1;
                    String[] str = {"Jan",
                            "Feb",
                            "Mar",
                            "Apr",
                            "May",
                            "Jun",
                            "Jul",
                            "Aug",
                            "Sep",
                            "Oct",
                            "Nov",
                            "Dec"};

                    tv.setText( "Mon, "+str[f]+" "+first[1]+" - "+"Sun, "+str[s]+" "+second[1]);
                    tv.setTypeface(typeNormal);

                    TextView tvs = (TextView)v.findViewById(R.id.feed_data);
                    tvs.setText(menuItems.get(pos).get(KEY_DESCRIPTION));

                    int i = (menuItems.get(pos).get(KEY_DESCRIPTION)).length();
                    image = (ImageView) v.findViewById(R.id.luck_bar);
                    random = new Random(i);
                    image.setImageResource(imageArray[random.nextInt(6)]);

                    tvs.setTypeface(typeBold);
                    return v;
                }};
            listView.setAdapter(adapter);
        }
    }

    public boolean hasConnection(Context context){
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()){
            return true;
        }
        NetworkInfo mobileNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()){
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()){
            return true;
        }
        return false;
    }

    public void showNetDisabledAlertToUser(final Context context){

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet);

//        dialog.setTitle("Error!!!");
        dialog.setCancelable(false);
        Button button = (Button) dialog.findViewById(R.id.cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        Button button2 = (Button) dialog.findViewById(R.id.enable);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
//							finish();
                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(dialogIntent);
            }
        });
        dialog.show();

    }
}
