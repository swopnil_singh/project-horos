package app.ssr.com.astrohoroscope.Signs;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import app.ssr.com.astrohoroscope.Adapter.ViewPagerAdapter;
import app.ssr.com.astrohoroscope.Fragments.AquariusInfo;
import app.ssr.com.astrohoroscope.Fragments.AriesInfo;
import app.ssr.com.astrohoroscope.Fragments.CancerInfo;
import app.ssr.com.astrohoroscope.Fragments.CapriconInfo;
import app.ssr.com.astrohoroscope.Fragments.GeminiInfo;
import app.ssr.com.astrohoroscope.Fragments.LibraInfo;
import app.ssr.com.astrohoroscope.Fragments.LioInfo;
import app.ssr.com.astrohoroscope.Fragments.PiscesInfo;
import app.ssr.com.astrohoroscope.Fragments.SagittariusInfo;
import app.ssr.com.astrohoroscope.Fragments.ScorpioInfo;
import app.ssr.com.astrohoroscope.Fragments.TaurusInfo;
import app.ssr.com.astrohoroscope.Fragments.VirgoInfo;
import app.ssr.com.astrohoroscope.R;

public class SignInformation extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;
    int[] image_id ={R.drawable.aries, R.drawable.tarus,R.drawable.gemini,R.drawable.cancer,
            R.drawable.leo,R.drawable.virgo,R.drawable.libra, R.drawable.scorpio,R.drawable.sagitarus,
            R.drawable.capricon,R.drawable.aquaries,R.drawable.pisces};
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_information);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sign Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new AriesInfo(),"Aries");
        viewPagerAdapter.addFragments(new TaurusInfo(),"Taurus");
        viewPagerAdapter.addFragments(new GeminiInfo(),"Gemini");
        viewPagerAdapter.addFragments(new CancerInfo(),"Cancer");
        viewPagerAdapter.addFragments(new LioInfo(),"Leo");
        viewPagerAdapter.addFragments(new VirgoInfo(),"Virgo");
        viewPagerAdapter.addFragments(new LibraInfo(),"Libra");
        viewPagerAdapter.addFragments(new ScorpioInfo(),"Scorpio");
        viewPagerAdapter.addFragments(new SagittariusInfo(),"Sagittarius");
        viewPagerAdapter.addFragments(new CapriconInfo(),"Capricon");
        viewPagerAdapter.addFragments(new AquariusInfo(),"Aquarius");
        viewPagerAdapter.addFragments(new PiscesInfo(),"Pisces");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        setIcons();

        //ads integration
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        // Real AdMob app ID: ca-app-pub-3584947486571209~6433088555
        MobileAds.initialize(this,"ca-app-pub-3584947486571209~6433088555");
        adView = (AdView) findViewById(R.id.adView);

        if(hasConnection(SignInformation.this)){
                //test on emulator
                //AdRequest adRequest =new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            //test on real device
            AdRequest adRequest =new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }
       else{
            adView.setVisibility(View.GONE);
        }

    }
    public void setIcons(){


        for(int i=0;i<12;i++){
            View view1 = getLayoutInflater().inflate(R.layout.customtab, null);
        view1.findViewById(R.id.icon).setBackgroundResource(image_id[i]);
        tabLayout.getTabAt(i).setCustomView(view1);
       }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sub,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case android.R.id.home:
                this.finish();
                break;
            case R.id.share_app:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "AstroHoroscope");
                    String msg = "\nTry out this cool Astrology app, it's awesome!!\n";
                    msg = msg + "https://play.google.com/store/apps/details?id=app.swopnil.com.astrohoroscope \n";
                    i.putExtra(Intent.EXTRA_TEXT, msg);
                    startActivity(Intent.createChooser(i, "Share using.."));
                } catch(Exception e) {
                    //e.toString();
                }break;
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean hasConnection(Context context){
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()){
            return true;
        }
        NetworkInfo mobileNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()){
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()){
            return true;
        }
        return false;
    }
}
