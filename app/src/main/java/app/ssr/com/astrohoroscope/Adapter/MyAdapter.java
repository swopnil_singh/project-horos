package app.ssr.com.astrohoroscope.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


import app.ssr.com.astrohoroscope.Signs.Aries;
import app.ssr.com.astrohoroscope.Model.Data;
import app.ssr.com.astrohoroscope.R;

/**
 * Created by Control on 5/20/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.DataViewHolder>{

    ArrayList<Data> data = new ArrayList<Data>();
    Context context;

    public MyAdapter(ArrayList<Data> data, Context context){
        this.data = data;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,parent,false);
        DataViewHolder viewHolder = new DataViewHolder(view,context,data);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {

        Data da = data.get(position);
        holder.imageSign.setImageResource(da.getImageID());
        holder.signText.setText(da.getSignName());
        holder.dobText.setText(da.getDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imageSign;
        TextView signText;
        TextView dobText;
        Context context;
        ArrayList<Data> datas;

        public DataViewHolder(View view, Context context, ArrayList<Data> datas){
            super(view);
            this.context = context;
            this.datas = datas;

            view.setOnClickListener(this);
            String fontPath = "fonts/NexaRustSansBlack.otf";
            String fontPath1 = "fonts/FredokaOne_Regular.ttf";

            Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
            Typeface tf1 = Typeface.createFromAsset(context.getAssets(), fontPath1);

            imageSign = (ImageView) view.findViewById(R.id.imageSign);
            signText = (TextView) view.findViewById(R.id.textSign);
            signText.setTypeface(tf);
            dobText = (TextView) view.findViewById(R.id.textSignDOB);
            dobText.setTypeface(tf1);
        }

        @Override
        public void onClick(View v) {
           int position = getAdapterPosition();

            Data data = this.datas.get(position);
            Intent intent = new Intent(this.context, Aries.class);
            intent.putExtra("sign",data.getSignName());
            this.context.startActivity(intent);


//            if(position==1) {
//                Intent intent = new Intent(this.context, Taurus.class);
//                this.context.startActivity(intent);
//            }
        }
    }
}
