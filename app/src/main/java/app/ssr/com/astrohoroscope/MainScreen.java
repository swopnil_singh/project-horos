package app.ssr.com.astrohoroscope;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

import app.ssr.com.astrohoroscope.Adapter.CustomTypefaceSpan;
import app.ssr.com.astrohoroscope.Adapter.MyAdapter;
import app.ssr.com.astrohoroscope.Compatibility.Compatible;
import app.ssr.com.astrohoroscope.Model.Data;
import app.ssr.com.astrohoroscope.Signs.SignInformation;

public class MainScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Insert your Application Package Name
    //private final static String PACKAGE_NAME = "app.swopnil.com.astrohoroscope";

    Toolbar toolbar;
    DrawerLayout drawer;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    int[] image_id ={R.drawable.aries, R.drawable.tarus,R.drawable.gemini,R.drawable.cancer,
            R.drawable.leo,R.drawable.virgo,R.drawable.libra, R.drawable.scorpio,R.drawable.sagitarus,
            R.drawable.capricon,R.drawable.aquaries,R.drawable.pisces};
    String[] sign_name, sign_dob;
    ArrayList<Data> list;

    AdView adView;
    private InterstitialAd interstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        toolbar = (Toolbar) findViewById(R.id.t_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Astro Zodiac");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        recyclerView  = (RecyclerView) findViewById(R.id.recyclerView);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        sign_name = getResources().getStringArray(R.array.signName);


        sign_dob = getResources().getStringArray(R.array.signDOB);
        list = new ArrayList<Data>();

        int count = 0;
        for(String name : sign_name){
            Data data = new Data(name, sign_dob[count],image_id[count]);
            count++;
            list.add(data);
        }

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter = new MyAdapter(list,this);
        recyclerView.setAdapter(adapter);


        interstitialAd = new InterstitialAd(this);
        //interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
       interstitialAd.setAdUnitId("ca-app-pub-3584947486571209/1590068799");
        interstitialAd.loadAd(new AdRequest.Builder().build());


        //-----------------------------------------
        if (hasConnection(MainScreen.this)){
            //call methods
            //getJsonData();

        }
        else{
            showNetDisabledAlertToUser(MainScreen.this);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
////        getMenuInflater().inflate(R.menu.main_screen, menu);
////        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_compat) {

            if(interstitialAd.isLoaded()){
                interstitialAd.show();

            }else{
            Intent intent2 = new Intent(getApplicationContext(), Compatible.class);
            startActivity(intent2);
            }
            interstitialAd.setAdListener(new AdListener()
                                         {
                                             @Override
                                             public void onAdClosed() {
                                                 super.onAdClosed();
                                                 Intent intent2 = new Intent(getApplicationContext(), Compatible.class);
                                                 startActivity(intent2);
                                                 interstitialAd.loadAd(new AdRequest.Builder().build());
                                             }
                                         }
            );


        } else if (id == R.id.nav_signInfo) {

            if(interstitialAd.isLoaded()){
                interstitialAd.show();

            }else{
                Intent intent = new Intent(getApplicationContext(), SignInformation.class);
                startActivity(intent);
            }
            interstitialAd.setAdListener(new AdListener()
                                         {
                                             @Override
                                             public void onAdClosed() {
                                                 super.onAdClosed();
                                                 Intent intent2 = new Intent(getApplicationContext(), SignInformation.class);
                                                 startActivity(intent2);
                                                 interstitialAd.loadAd(new AdRequest.Builder().build());
                                             }
                                         }
            );




        } else if (id == R.id.nav_rateUs) {
            if(interstitialAd.isLoaded()){
                interstitialAd.show();

            }else{
                try
                {
                    Intent rateIntent = rateIntentForUrl("market://details");
                    startActivity(rateIntent);
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+ getApplicationContext().getPackageName())));
                }
                catch (ActivityNotFoundException e)
                {
                    Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
                    startActivity(rateIntent);
                }
            }
            interstitialAd.setAdListener(new AdListener()
            {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    try
                    {
                        Intent rateIntent = rateIntentForUrl("market://details");
                        startActivity(rateIntent);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details" +getApplicationContext().getPackageName())));
                    }
                    catch (ActivityNotFoundException e)
                    {
                        Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
                        startActivity(rateIntent);
                    }
                    interstitialAd.loadAd(new AdRequest.Builder().build());

                }
            });

        } else if (id == R.id.nav_aboutUs) {
            if(interstitialAd.isLoaded()){
                interstitialAd.show();

            }else{
                contactUs();
            }
            interstitialAd.setAdListener(new AdListener()
            {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    contactUs();
                    interstitialAd.loadAd(new AdRequest.Builder().build());
                }
            });

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public boolean hasConnection(Context context){
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()){
            return true;
        }
        NetworkInfo mobileNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()){
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()){
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    public void showNetDisabledAlertToUser(final Context context){

        final Dialog dialog = new Dialog(MainScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet);

//        dialog.setTitle("Error!!!");
        dialog.setCancelable(false);
        Button button = (Button) dialog.findViewById(R.id.cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MainScreen.this.finish();
            }
        });
        Button button2 = (Button) dialog.findViewById(R.id.enable);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
//							finish();
                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(dialogIntent);
            }
        });
        dialog.show();
    }

    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    private void contactUs() {
        Intent sharingIntent = new Intent(Intent.ACTION_SENDTO);
        sharingIntent.setData(Uri.parse("mailto:"));
//        sharingIntent.setType("message/rfc822");
        String shareBody = "Enter message here...";
        String subject = "Astro App";


        sharingIntent.putExtra(Intent.EXTRA_EMAIL,new String[]{"appastrozodiac@gmail.com"});
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Contact Via.."));
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/mon.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0, mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
