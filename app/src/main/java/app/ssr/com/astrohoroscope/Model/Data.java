package app.ssr.com.astrohoroscope.Model;

/**
 * Created by Control on 5/22/2017.
 */

public class Data {
    public String signName;
    public String date;
    public int imageID;

    public Data(String signName, String date, int imageID) {
        this.setImageID(imageID);
        this.setDate(date);
        this.setSignName(signName);
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }
}
