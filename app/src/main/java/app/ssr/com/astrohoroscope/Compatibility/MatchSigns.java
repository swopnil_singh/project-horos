package app.ssr.com.astrohoroscope.Compatibility;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.sql.SQLException;

import app.ssr.com.astrohoroscope.Adapter.DataBaseHelper;
import app.ssr.com.astrohoroscope.R;



public class MatchSigns extends AppCompatActivity {

    Toolbar _toolbar;
    TextView _textFriend, _textLove;
    Bundle youIntent,partnerIntent;
    String you,partner;
    Typeface typeNormal,typeBold;

    Cursor c;
    DataBaseHelper myDbHelper;

    AdView adView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_signs);

        _toolbar = (Toolbar) findViewById(R.id.toolsBars);
         setSupportActionBar(_toolbar);


        _textFriend = (TextView) findViewById(R.id.friend_comp);
        _textLove = (TextView) findViewById(R.id.love_comp);

        youIntent = getIntent().getExtras();
        you = youIntent.getString("You");

        partnerIntent = getIntent().getExtras();
        partner = partnerIntent.getString("Partner");

        getSupportActionBar().setTitle(you+" and "+partner);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        typeNormal = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/mon.otf");
        typeBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/FredokaOne_Regular.ttf");


        //ads integration
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        // Real AdMob app ID: ca-app-pub-3584947486571209~6433088555
        MobileAds.initialize(this,"ca-app-pub-3584947486571209~6433088555");
        adView = (AdView) findViewById(R.id.adView);

        if(hasConnection(MatchSigns.this)){
            adView.setVisibility(View.VISIBLE);
                //test on emulator
                //AdRequest adRequest =new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            //test on real device
            AdRequest adRequest =new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }
        else {
            adView.setVisibility(View.GONE);
        }



        //databasehelper
        myDbHelper = new DataBaseHelper(this);
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        dataHelpFriend();
        dataHelpLove();
    }

    public void dataHelpFriend(){
        condition();
        c = myDbHelper.getData("comp_friend",you.toLowerCase()+"_"+partner.toLowerCase());
        if (c.moveToFirst()) {
            do {
                _textFriend.setText(c.getString(2));
                _textFriend.setTypeface(typeNormal);
            } while (c.moveToNext());

        }
    }

    public void dataHelpLove(){
        condition();
        c = myDbHelper.getData("comp_love",you.toLowerCase()+"_"+partner.toLowerCase());
        if (c.moveToFirst()) {
            do {
                _textLove.setText(c.getString(2));
                _textLove.setTypeface(typeNormal);
            } while (c.moveToNext());

        }
    }

    public void condition(){
        if(you.equals("Taurus") && partner.equals("Aries")){
            you = "Aries";
            partner = "Taurus";
        }

        if(you.equals("Gemini") && partner.equals("Aries")){
            you = "Aries";
            partner = "Gemini";
        }
        if(you.equals("Gemini") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Gemini";
        }

        if(you.equals("Cancer") && partner.equals("Aries")){
            you = "Aries";
            partner = "Cancer";
        }
        if(you.equals("Cancer") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Cancer";
        }
        if(you.equals("Cancer") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Cancer";
        }

        if(you.equals("Leo") && partner.equals("Aries")){
            you = "Aries";
            partner = "Leo";
        }
        if(you.equals("Leo") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Leo";
        }
        if(you.equals("Leo") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Leo";
        }
        if(you.equals("Leo") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Leo";
        }

        if(you.equals("Virgo") && partner.equals("Aries")){
            you = "Aries";
            partner = "Virgo";
        }
        if(you.equals("Virgo") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Virgo";
        }
        if(you.equals("Virgo") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Virgo";
        }
        if(you.equals("Virgo") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Virgo";
        }
        if(you.equals("Virgo") && partner.equals("Leo")){
            you = "Leo";
            partner = "Virgo";
        }

        if(you.equals("Libra") && partner.equals("Aries")){
            you = "Aries";
            partner = "Libra";
        }
        if(you.equals("Libra") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Libra";
        }
        if(you.equals("Libra") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Libra";
        }
        if(you.equals("Libra") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Libra";
        }
        if(you.equals("Libra") && partner.equals("Leo")){
            you = "Leo";
            partner = "Libra";
        }
        if(you.equals("Libra") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Libra";
        }

        if(you.equals("Scorpio") && partner.equals("Aries")){
            you = "Aries";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Leo")){
            you = "Leo";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Scorpio";
        }
        if(you.equals("Scorpio") && partner.equals("Libra")){
            you = "Libra";
            partner = "Scorpio";
        }

        if(you.equals("Sagittarius") && partner.equals("Aries")){
            you = "Aries";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Leo")){
            you = "Leo";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Libra")){
            you = "Libra";
            partner = "Sagittarius";
        }
        if(you.equals("Sagittarius") && partner.equals("Scorpio")){
            you = "Scorpio";
            partner = "Sagittarius";
        }

        if(you.equals("Capricorn") && partner.equals("Aries")){
            you = "Aries";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Leo")){
            you = "Leo";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Libra")){
            you = "Libra";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Scorpio")){
            you = "Scorpio";
            partner = "Capricorn";
        }
        if(you.equals("Capricorn") && partner.equals("Sagittarius")){
            you = "Sagittarius";
            partner = "Capricorn";
        }

        if(you.equals("Aquarius") && partner.equals("Aries")){
            you = "Aries";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Leo")){
            you = "Leo";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Libra")){
            you = "Libra";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Scorpio")){
            you = "Scorpio";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Sagittarius")){
            you = "Sagittarius";
            partner = "Aquarius";
        }
        if(you.equals("Aquarius") && partner.equals("Capricorn")){
            you = "Capricorn";
            partner = "Aquarius";
        }

        if(you.equals("Pisces") && partner.equals("Aries")){
            you = "Aries";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Taurus")){
            you = "Taurus";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Gemini")){
            you = "Gemini";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Cancer")){
            you = "Cancer";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Leo")){
            you = "Leo";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Virgo")){
            you = "Virgo";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Libra")){
            you = "Libra";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Scorpio")){
            you = "Scorpio";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Sagittarius")){
            you = "Sagittarius";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Aquarius")){
            you = "Aquarius";
            partner = "Pisces";
        }
        if(you.equals("Pisces") && partner.equals("Capricorn")){
            you = "Capricorn";
            partner = "Pisces";
        }
    }
    public boolean hasConnection(Context context){
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()){
            return true;
        }
        NetworkInfo mobileNetwork=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()){
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()){
            return true;
        }
        return false;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case android.R.id.home:
                this.finish();
                break;
//            case R.id.sign_info:
//                Toast.makeText(this, "Hello share", Toast.LENGTH_SHORT).show();;
        }
        return super.onOptionsItemSelected(item);
    }
}
