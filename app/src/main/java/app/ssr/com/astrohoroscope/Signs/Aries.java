package app.ssr.com.astrohoroscope.Signs;



import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

//import app.swopnil.com.astrohoroscope.Adapter.ViewPagerAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import app.ssr.com.astrohoroscope.DailyFragment;
import app.ssr.com.astrohoroscope.MonthlyFragment;
import app.ssr.com.astrohoroscope.R;
//import app.swopnil.com.astrohoroscope.SharedPref;
import app.ssr.com.astrohoroscope.WeeklyFragment;


public class Aries extends AppCompatActivity {

    Toolbar toolbar;
    String daily,weekly,monthly,ids;
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aries);


        toolbar = (Toolbar) findViewById(R.id.toolbarApp);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorToolbar)));

        //Add back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ads integration
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        // Real AdMob app ID: ca-app-pub-3584947486571209~6433088555
        MobileAds.initialize(this,"ca-app-pub-3584947486571209~6433088555");
        adView = (AdView) findViewById(R.id.adView);
            //test on emulator
            //AdRequest adRequest =new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        //test on real device
        AdRequest adRequest =new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        String sign = intent.getStringExtra("sign");

        switch (sign) {

            case "Aries":
                getSupportActionBar().setTitle("Aries");

                daily = "http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Aries&id=45";
                weekly = "http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Aries&id=45";
                monthly = "http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Aries&id=45";
                ids = "0";
                dataSendFragment();
                break;

            case "Taurus":
                getSupportActionBar().setTitle("Taurus");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Taurus&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Taurus&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Taurus&id=45";
                ids = "1";
                dataSendFragment();
                break;

            case "Aquarius":
                getSupportActionBar().setTitle("Aquarius");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Aquarius&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Aquarius&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Aquarius&id=45";
                ids = "10";
                dataSendFragment();
                break;

            case "Virgo":
                getSupportActionBar().setTitle("Virgo");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Virgo&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Virgo&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Virgo&id=45";
                ids = "5";
                dataSendFragment();
                break;
            case "Scorpio":
                getSupportActionBar().setTitle("Scorpio");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Scorpio&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Scorpio&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Scorpio&id=45";
                ids = "7";
                dataSendFragment();
                break;

            case "Sagittarius":
                getSupportActionBar().setTitle("Sagittarius");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Sagittarius&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Sagittarius&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Sagittarius&id=45";
                ids = "8";
                dataSendFragment();
                break;

            case "Libra":
                getSupportActionBar().setTitle("Libra");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Libra&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Libra&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Libra&id=45";
                ids = "6";
                dataSendFragment();
                break;

            case "Leo":
                getSupportActionBar().setTitle("Leo");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Leo&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Leo&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Leo&id=45";
                ids = "4";
                dataSendFragment();
                break;

            case "Gemini":
                getSupportActionBar().setTitle("Gemini");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Gemini&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Gemini&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Gemini&id=45";
                ids = "2";
                dataSendFragment();
                break;

            case "Cancer":
                getSupportActionBar().setTitle("Cancer");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Cancer&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Cancer&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Cancer&id=45";
                ids = "3";
                dataSendFragment();
                break;

            case "Pisces":
                getSupportActionBar().setTitle("Pisces");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Pisces&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Pisces&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Pisces&id=45";
                ids = "11";
                dataSendFragment();
                break;

            case "Capricorn":
                getSupportActionBar().setTitle("Capricorn");
                //Set preferences to store url
                daily="http://www.findyourfate.com/rss/dailyhoroscope-feed.php?sign=Capricorn&id=45";
                weekly="http://www.findyourfate.com/rss/weekly-horoscope.php?sign=Capricorn&id=45";
                monthly="http://www.findyourfate.com/rss/monthly-horoscope.php?sign=Capricorn&id=45";
                ids = "9";
                dataSendFragment();
                break;

        }


    }

    public void dataSendFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("dailyUrl",daily);

        Bundle bundle1 = new Bundle();
        bundle1.putString("weeklyUrl",weekly);

        Bundle bundle2 = new Bundle();
        bundle2.putString("monthlyUrl",monthly);

        Bundle bundle3 = new Bundle();
        bundle.putString("ids",ids);


        DailyFragment dailyfragment = new DailyFragment();
        dailyfragment.setArguments(bundle);
        FragmentManager  manager = getSupportFragmentManager();
        manager.beginTransaction()
                .add(R.id.daily_frame, dailyfragment,dailyfragment.getTag())
                .commit();

        WeeklyFragment weeklyfragment = new WeeklyFragment();
        weeklyfragment.setArguments(bundle1);
        manager.beginTransaction()
                .add(R.id.weekly_fragment, weeklyfragment,weeklyfragment.getTag())
                .commit();

        MonthlyFragment monthlyfragment = new MonthlyFragment();
        monthlyfragment.setArguments(bundle2);
        manager.beginTransaction()
                .add(R.id.monthly_fragment, monthlyfragment,monthlyfragment.getTag())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sub,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case android.R.id.home:
                this.finish();
                break;
            case R.id.share_app:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "AstroHoroscope");
                    String msg = "\nTry out this cool Astrology app, it's awesome!!\n";
                    msg = msg + "https://play.google.com/store/apps/details?id=app.swopnil.com.astrohoroscope \n";
                    i.putExtra(Intent.EXTRA_TEXT, msg);
                    startActivity(Intent.createChooser(i, "Share using.."));
                } catch(Exception e) {
                    //e.toString();
                }break;
        }
        return super.onOptionsItemSelected(item);
    }


}
