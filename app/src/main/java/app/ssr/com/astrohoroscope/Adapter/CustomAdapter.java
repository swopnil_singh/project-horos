package app.ssr.com.astrohoroscope.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import app.ssr.com.astrohoroscope.R;


/**
 * Created by swopnil on 7/11/17.
 */

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private String[] names;
    private int[] images;


    public CustomAdapter(Context context, String[] names, int[] images){
        this.context = context;
        this.names = names;
        this.images = images;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int i) {
        return names[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflate.inflate(R.layout.custom, null);
        }

        TextView txt = (TextView) view.findViewById(R.id.text_custom);
        ImageView img = (ImageView) view.findViewById(R.id.image_custom);

        txt.setText(names[i]);
        img.setImageResource(images[i]);

        return view;
    }
}
