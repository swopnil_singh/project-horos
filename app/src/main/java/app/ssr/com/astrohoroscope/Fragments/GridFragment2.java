package app.ssr.com.astrohoroscope.Fragments;

import android.app.DialogFragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import app.ssr.com.astrohoroscope.Adapter.CustomAdapter;
import app.ssr.com.astrohoroscope.R;


/**
 * Created by swopnil on 7/11/17.
 */

public class GridFragment2 extends DialogFragment {


    GridView gridView;
    Resources res;
    String[] names;
    int[] images ={R.drawable.aries, R.drawable.tarus,R.drawable.gemini,R.drawable.cancer,
            R.drawable.leo,R.drawable.virgo,R.drawable.libra, R.drawable.scorpio,R.drawable.sagitarus,
            R.drawable.capricon,R.drawable.aquaries,R.drawable.pisces};

    public interface CustomDialogInterFace2 {
        void itemSelect2(String name);
    }

    public GridFragment2(){

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.grid,null);
        gridView = (GridView) view.findViewById(R.id.grid_view);

        res = getResources();
        names = res.getStringArray(R.array.signName);

        getDialog().setTitle("Select one");
        CustomAdapter adapter = new CustomAdapter(getActivity(),names,images);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                GridFragment2.CustomDialogInterFace2 activity = (GridFragment2.CustomDialogInterFace2) getActivity();
                activity.itemSelect2(names[i]);
                dismiss();
            }
        });

        return view;
    }
}
