package app.ssr.com.astrohoroscope.Fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.ssr.com.astrohoroscope.R;

/**
 * Created by Control on 6/14/2017.
 */

public class LioInfo extends Fragment {
    TextView[] text;
    TextView title;
    String[] data;
    Typeface typeNormal,typeBold;

    TextView info;
    String datas;
    ImageView imageCeleb;
    public LioInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewRoot = inflater.inflate(R.layout.fragment_aries_info, container, false);

        typeNormal = Typeface.createFromAsset(getContext().getAssets(), "fonts/mon.otf");
        typeBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/FredokaOne_Regular.ttf");
        title = (TextView) viewRoot.findViewById(R.id.signName);

        title.setText("Leo(The Lion)");
        title.setTypeface(typeBold);


        data = getResources().getStringArray(R.array.LeoInfo);
        int[] textID ={R.id.element_info,R.id.quality_info,R.id.color_info,R.id.ruler_info,
                R.id.stone_info,R.id.day_info,R.id.great_info,R.id.number_info,R.id.date_info,
                R.id.secret_info,R.id.spot_info,R.id.strength_info,R.id.weakness_info,R.id.likes_info,
                R.id.dislike_info};


        text = new TextView[textID.length];
        for(int i =0; i<data.length; i++){
            text[i] = (TextView) viewRoot.findViewById(textID[i]);
        }

        for (int a =0;a<text.length;a++){
            text[a].setText(data[a]);
            text[a].setTypeface(typeNormal);
        }
        info = (TextView) viewRoot.findViewById(R.id.info_info);
        datas = getResources().getString(R.string.leo_info);

        imageCeleb = (ImageView) viewRoot.findViewById(R.id.celeb_pics);
        imageCeleb.setImageResource(R.drawable.leo_celeb);

        info.setTypeface(typeNormal);
        info.setText(datas);


        return viewRoot;
    }

}

