package app.ssr.com.astrohoroscope.Compatibility;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import app.ssr.com.astrohoroscope.Fragments.GridFragment;
import app.ssr.com.astrohoroscope.Fragments.GridFragment2;
import app.ssr.com.astrohoroscope.R;

public class Compatible extends AppCompatActivity implements GridFragment.CustomDialogInterFace, GridFragment2.CustomDialogInterFace2 {

    Toolbar toolbar;
    Cursor c;
    TextView _textView;
    Button _you,_partner;
    String nameYou,namePartner;
    AdView adView;
    Vibrator vibe;
    Typeface typeNormal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compatible);

        toolbar = (Toolbar) findViewById(R.id.toolsBars);
        toolbar.setTitle("Compatibility");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _you = (Button) findViewById(R.id.btn_you);
        _partner = (Button) findViewById(R.id.btn_partner);
        typeNormal = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/FredokaOne_Regular.ttf");
        _you.setTypeface(typeNormal);
        _partner.setTypeface(typeNormal);

        _textView = (TextView) findViewById(R.id.textName);

        nameYou = _you.getText().toString();
        namePartner = _partner.getText().toString();
        vibe = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE) ;
        //ads integration
            // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
            // Real AdMob app ID: ca-app-pub-3584947486571209~6433088555
        MobileAds.initialize(this,"ca-app-pub-3584947486571209~6433088555");
        adView = (AdView) findViewById(R.id.adView);
            //test on emulator
            //AdRequest adRequest =new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        //test on real device
        AdRequest adRequest =new AdRequest.Builder().build();
         adView.loadAd(adRequest);

//

    }

    @Override
    public void itemSelect(String name) {
        nameYou = name;
        _you.setText(nameYou);

    }

    @Override
    public void itemSelect2(String name) {
        namePartner = name;
        _partner.setText(namePartner);
    }

    public void goTo(View view) {
        final FragmentManager fragmentManager = getFragmentManager();
        GridFragment gridFragment = new GridFragment();
        gridFragment.show(fragmentManager,"signs");
    }

    public void goToSec(View view) {
        final FragmentManager fragmentManager = getFragmentManager();
        GridFragment2 gridFragment = new GridFragment2();
        gridFragment.show(fragmentManager,"signs");
    }

    public void gotTOMatch(View view) {
            vibe.vibrate(50);
            Intent intent = new Intent(getApplicationContext(), MatchSigns.class);
            intent.putExtra("You", nameYou);
            intent.putExtra("Partner", namePartner);
            startActivity(intent);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case android.R.id.home:
                this.finish();
                break;
//            case R.id.sign_info:
//                Toast.makeText(this, "Hello share", Toast.LENGTH_SHORT).show();;
        }
        return super.onOptionsItemSelected(item);
    }

}

