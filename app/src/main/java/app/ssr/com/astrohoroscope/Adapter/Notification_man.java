package app.ssr.com.astrohoroscope.Adapter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import app.ssr.com.astrohoroscope.MainScreen;
import app.ssr.com.astrohoroscope.R;

/**
 * Created by swopnil on 8/3/17.
 */

public class Notification_man extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


//        Intent repeating = new Intent(context, MainScreen.class);
//        repeating.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);



//        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
//                0, PendingIntent.FLAG_UPDATE_CURRENT );

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle("Astro Alert!")
                .setContentText("Your daily dose of Horoscope is ready.")
                .setSound(alarmSound)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(true);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builder.setSmallIcon(R.drawable.notifi_transparent);
        }else{
            builder.setSmallIcon(R.drawable.notifi);
        }


        Intent resultIntent = new Intent(context, MainScreen.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainScreen.class);
        stackBuilder.addNextIntent(resultIntent);


        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT );
        builder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(100, builder.build());

    }
}
