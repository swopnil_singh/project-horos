package app.ssr.com.astrohoroscope.Model;

/**
 * Created by Control on 5/24/2017.
 */

public class RssData {
    private String signDescription;
    private String dateDiscription;


    public String getSignDescription() {
        return signDescription;
    }

    public void setSignDescription(String signDescription) {
        this.signDescription = signDescription;
    }

    public String getDateDiscription() {
        return dateDiscription;
    }

    public void setDateDiscription(String dateDiscription) {
        this.dateDiscription = dateDiscription;
    }
}
