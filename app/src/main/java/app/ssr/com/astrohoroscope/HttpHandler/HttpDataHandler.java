package app.ssr.com.astrohoroscope.HttpHandler;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Control on 6/4/2017.
 */

public class HttpDataHandler {

    static String stream = null;

    public HttpDataHandler(){

    }

    public String GetHttpData(String stringUrl){
        try{
            URL url = new URL(stringUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStream is =new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null){
                    stringBuilder.append(line);
                    stream = stringBuilder.toString();
                    urlConnection.disconnect();
                }
            }

        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }return stream;
    }
}
